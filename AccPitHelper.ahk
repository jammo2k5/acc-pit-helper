#NoEnv
#SingleInstance, off
#NoTrayIcon
SetMouseDelay, -1

if( %0% > 0 ){
    func = %1%
    amount = %2%
    amount := StrReplace(amount, "d", "-")
    amount := StrReplace(amount, "u", "+")
    acc := new accdata()
    func:= strreplace(func,"","")

    if(func = "fuel")
        acc.setfuel(amount)

    if(func = "pressure")
        acc.bumptyprepressures(amount)

    if(func = "brakes")
        acc.setbrakes(amount)
}
exitapp

class accdata {
    tyrepage := {x:217,y:241}
    tyrepressures := {front:504,rear:1020,upleft:701,upright:1490,dnleft:572,dnright:1357}
    fuelpagepos := {x:589,y:241}
    fuelslider := {upx:1204,dnx:990,fuely:431,fbrakesy:574,rbrakesy:619}
    accscalefactor := null
    accxoffset := null

    accname := "ahk_exe AC2-Win64-Shipping.exe"

    __New(){
        this.getwindow()
    }

    getwindow(){
        winname := this.accname
        WinGetPos, accx, accy, accw, acch, %winname%
        this.accxoffset := (accw - (acch / 9 * 16)) / 2
        this.accscalefactor := acch / 1440
        OutputDebug, % accw
    }

    clickacc(x,y){
        x := (x * this.accscalefactor) + this.accxoffset
        y := y * this.accscalefactor
        winname := this.accname
        SetControlDelay -1
        SetMouseDelay, -1
        controlclick,x%x% y%y%,%winname%,,,,NA
    }

    bumptyprepressures(amount){
        this.getwindow()
        this.clickacc(this.tyrepage.x,this.tyrepage.y)
        sleep 300
        loops := amount > 0 ? amount : amount * - 1
        OutputDebug, % amount
        OutputDebug, % loops
        if(amount != 0){
            loop %loops%{
                this.clickacc(amount > 0 ? this.tyrepressures.upleft : this.tyrepressures.dnleft ,this.tyrepressures.front)
                this.clickacc(amount > 0 ? this.tyrepressures.upright : this.tyrepressures.dnright ,this.tyrepressures.front)
                this.clickacc(amount > 0 ? this.tyrepressures.upleft : this.tyrepressures.dnleft ,this.tyrepressures.rear)
                this.clickacc(amount > 0 ? this.tyrepressures.upright : this.tyrepressures.dnright ,this.tyrepressures.rear)
            }
        }
    }

    setfuel(amount){
        this.clickacc(this.fuelpagepos.x,this.fuelpagepos.y)
        sleep 300
        if(instr(amount,"+") OR instr(amount,"-")){
            loops := amount > 0 ? amount : amount * - 1
            if(amount != 0){
                loop %loops%{
                    this.clickacc(amount > 0 ? this.fuelslider.upx : this.fuelslider.dnx ,this.fuelslider.fuely)
                }
            }
        }
        else{
            loop 108{
                this.clickacc(this.fuelslider.dnx,this.fuelslider.fuely)
            }
            loops := amount - 2
            loop %loops% {
                this.clickacc(this.fuelslider.upx,this.fuelslider.fuely)
            }
        }
    }

    setbrakes(amount){
        this.clickacc(this.fuelpagepos.x,this.fuelpagepos.y)
        sleep 300
        loop 3{
            this.clickacc(this.fuelslider.dnx,this.fuelslider.fbrakesy)
            this.clickacc(this.fuelslider.dnx,this.fuelslider.rbrakesy)
        }
        loops := amount - 1
        loop %loops% {
            this.clickacc(this.fuelslider.upx,this.fuelslider.fbrakesy)
            this.clickacc(this.fuelslider.upx,this.fuelslider.rbrakesy)
        }
    }
}
