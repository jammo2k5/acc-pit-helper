# **ACC Pit Helper**
Acc Pit helper is a macro set for ACC to help with setups before a session.
It currently has the ability to:
- Change typre pressures of all 4 tyres at once by any amount of clicks given.
- Set the brake pads to any compound from 1-4.
- Set a specific amount of fuel or change the fuel by any amount given.

## **Usage**
run the exe with command line arguments to get the desired outcome e.g.
>`"ACCpithelper.exe" [command] [(u/d)4]`

## **Functions**

### **Tyre Pressures**
`pressure [u,d][number]`
bumps all 4 typre pressures by the given amount either up (u) or down (d).

Usage example:
>`"ACCpithelper.exe" pressure d5`

moves all tyre pressures down 5 clicks.

### **Fuel**
`fuel ([u,d])[number]`
Bumps or sets the fuel value to/by the given amount
If a d or u are omitted from before the number it will set the fuel to that exact amount of litres (range 2-110) and if a d or u are present it will bump the fuel up or doen by the given amount.
Usage example 1:
>`"ACCpithelper.exe" fuel d5`

moves fuel down 5 litres.

Usage example 2:
>`"ACCpithelper.exe" fuel 50`

Sets the fuel to 50 litres.

### **Brakes**
`Brakes [number]`
Sets the brakes to the given compound number (range 1-4).

Usage example:
>`"ACCpithelper.exe" brakes 1`

Sets the brakes to compound 1.